import React, {useState} from 'react';
import './App.css';
import Homepage from './pages/Homepage';
import { BrowserRouter as Router,Route,Routes } from "react-router-dom";
import KeyboardData from './components/keyboard/KeyboardData.js'
import KeycapData from './components/keyboard/KeycapData.js';
import Cart from './components/cart/Cart';
import ProductPage from './pages/ProductPage';
import Swal from 'sweetalert2';
import Login from './pages/userpage/Login';
import Register from './pages/userpage/Register';
import PayCheck from './pages/orderpage/PayCheck';
import AccountPage from './pages/accountpage/AccountPage';

const App = () => {
  /*
    restfull API + Spring boot + thanh toán vnpay
    */
    const {productItems} = KeyboardData;
    const {productKCItems} = KeycapData;
    const [cartItems, setCardItems] = useState([])
    // Thêm sản phẩm vào giỏ hàng
    const addToCart = (product) => {
      const productKB = cartItems.find((item) => item.idProduct === product.idProduct)

      if(productKB){
        setCardItems(cartItems.map((item) => (item.idProduct === product.idProduct ? {
          ...productKB, qty: productKB.qty + 1
        } : item)))
      }else{
        setCardItems([...cartItems, {...product, qty: 1}])
      }
      Swal.fire(
        'Good job!',
        'Đã thêm sản phẩm vào giỏ!',
        'success'
      )
    }
    // Tăng sản phẩm
    const increaseQty = (product) => {
      const productKB = cartItems.find((item) => item.idProduct === product.idProduct)

      if(productKB){
        setCardItems(cartItems.map((item) => (item.idProduct === product.idProduct ? {
          ...productKB, qty: productKB.qty + 1
        } : item)))
      }else{
        setCardItems([...cartItems, {...product, qty: 1}])
      }
    }
    // giảm sản phẩm trong giỏ
    const decreaseQty = (product) => {
      const productKB = cartItems.find((item) => item.idProduct === product.idProduct)

      if(productKB.qty === 1){
        setCardItems(cartItems.filter((item) => item.idProduct !== product.idProduct))
      }else{
        setCardItems(cartItems.map((item) => (item.idProduct === product.idProduct ? {...productKB, qty: productKB.qty - 1} : item)))
      }
    }

    //xóa sản phẩm
    const removeCartItem = (product) => {
      setCardItems(cartItems.filter((item) => item.idProduct !== product.idProduct));
    }
  
    return (
      <Router>
        <Routes>
        <Route path='/' element={<Homepage productItems={productItems} productKCItems={productKCItems} addToCart={addToCart} cartItems={cartItems} />}/>
        <Route path='/cart' element={<Cart cartItems={cartItems} increaseQty={increaseQty} decreaseQty={decreaseQty} removeCartItem={removeCartItem}/>}/>
        <Route path='/product/:productId' element={<ProductPage productItems={productItems} productKCItems={productKCItems} addToCart={addToCart} cartItems={cartItems}/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/register' element={<Register/>}/>
        <Route path='/paycheck' element={<PayCheck cartItems={cartItems}/>}/>
        <Route path='/account/:loginLocal' element={<AccountPage/>}/>
      </Routes>
      </Router>
    );
  
}

export default App;
