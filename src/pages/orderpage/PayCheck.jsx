import React, { useState } from 'react'
import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';
import { Link } from 'react-router-dom';
import "./PayCheck.css"
import axios from '../../api/axios';


const PayCheck = ({ cartItems }) => {
    const [selectPayMethod, setSelectPayMethod] = useState(null);


    const storedItems = localStorage.getItem('getItemsInCart');
    const totalPriceCart = localStorage.getItem('totalPrice');
    const parseStoredItems = JSON.parse(storedItems);

    const handleClickOption = (option) => {
        setSelectPayMethod(option);
    }

    const itemToBackend = parseStoredItems.map(item => ({
        idProduct: item.idProduct,
        qty: item.qty,
        price: item.price
    }))


    const handlePayment = async (e) => {
        e.preventDefault();

        const data = {
            payMethod: selectPayMethod,
            total: totalPriceCart,
            items: itemToBackend
        };

        try {
            const response = await axios.post("/add/cart", data);
            console.log(response.data);
            localStorage.removeItem('getItemsInCart');
            localStorage.removeItem('totalPrice');
        }catch(err) {

        }
    }

    return (
        <>
            <Header cartItems={cartItems} />
            <div className="container mt-5">
                <div className="row">
                    <div className="col-6 col-infor-user">
                        <section className='infomation-user'>
                            <h2 className='text-center'>Thôn tin vận chuyển</h2>

                            <form action="">


                                <label htmlFor="toUser">Tên người nhận</label>
                                <input type="text" id='toUser' />



                                <label htmlFor="email">Email</label>
                                <input type="text" id='email' />



                                <label htmlFor="tel">Số điện thoại nhận hàng</label>
                                <input type="text" id='tel' />


                                <label htmlFor="address">Địa chỉ nhận hàng</label>
                                <textarea name="" id="address" cols="67" rows="10"></textarea>

                            </form>
                        </section>
                    </div>
                    <div className="col-5 col-infor-order">
                        <section className='information-order'>
                            <h2 className='text-center'>Thông tin đơn hàng</h2>

                            {parseStoredItems.map(item => (
                                <div className="pay-infor" key={item.idProduct}>
                                    <div className="img">
                                        <img className='img-product' src={item.src} alt="" />
                                    </div>
                                    <div className="cart-details">
                                        <p>Sản phẩm: {item.nameProduct}</p>
                                        <p>Giá: {item.priceProduct}</p>
                                        <p>Số lượng: {item.qty}</p>
                                        <p>Thành tiền: {
                                            item.qty * item.priceProduct
                                        }</p>
                                    </div>
                                </div>
                            ))}
                        </section>
                        <div className="cart-summary">
                            <div className="pay-option">
                                <div className={`cod ${selectPayMethod === 'cod' ? 'selected' : ''}`}
                                onClick={() => handleClickOption('cod')}></div>
                                <div className={`vnpay ${selectPayMethod === 'vnpay' ? 'selected' : ''}`}
                                onClick={() => handleClickOption('vnpay')}></div>
                            </div>
                            <p>Tổng tiền đơn hàng : {totalPriceCart}</p>
                            <button className='order btn-primary' onClick={handlePayment}>Đặt hàng</button>
                        </div>
                    </div>
                </div>
                <div className='mt-4'>
                    <Link to={"/cart"}>
                    <button className='btn btn-danger'>Quay lại giỏ hàng</button>
                    </Link>
                    
                </div>
            </div>
            <Footer />
        </>
    )
}

export default PayCheck