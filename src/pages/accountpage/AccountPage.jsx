import React from 'react'
import "./AccountPage.css"

const AccountPage = () => {
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-3 user-category">
            category
          </div>
          <div className="col-8 user-content">
            Info
          </div>
        </div>
      </div>
    </>
  )
}

export default AccountPage