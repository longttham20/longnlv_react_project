import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Header from '../components/header/Header'
import Footer from '../components/footer/Footer'
import axios from '../api/axios'
const ProductPage = ({ addToCart,cartItems }) => {
    const [productDetail, setProductDetail] = useState(null);
    const { productId } = useParams();

    const PRODUCT_DETAIL_API = "http://localhost:8080/product/";

    useEffect(() => {
        fetchData();
    },[productId]);

    const fetchData = async () => {
        try {
            const response = await axios.get(PRODUCT_DETAIL_API + productId);
            setProductDetail(response.data[0]);
        }catch(err) {
            console.error("Error fetching product data",err);
        }
    };
    if (!productDetail) {
        return <div>Không tìm thấy sản phẩm</div>;
    }

    return (
        <div>
            <Header cartItems={cartItems}/>
            <div className="container my-5 py-3">
                <div className="row">
                    <div className="col-md-6 d-flex justify-content-center mx-auto product">
                        <img src={productDetail.src} alt={productDetail.nameProduct} height={400} />
                    </div>
                    <div className="col-md-6 d-flex flex-column justify-content-center">
                        <h1 className="display-5 fw-bold">{productDetail.nameProduct}</h1>
                        <div className="rate">
                            <i className='fa fa-star'></i>
                            <i className='fa fa-star'></i>
                            <i className='fa fa-star'></i>
                            <i className='fa fa-star'></i>
                            <i className='fa fa-star'></i>
                        </div>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                            Voluptatibus iusto sunt veniam exercitationem ad saepe, vero voluptatem,
                            quae dolorem omnis culpa asperiores necessitatibus consectetur!
                            Ea inventore repudiandae eaque omnis dolores?</p>
                        <hr />
                        <h2 className="my-4">${productDetail.priceProduct}</h2>

                        <button onClick={() => addToCart(productDetail)}>
                            <i className="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
}

export default ProductPage