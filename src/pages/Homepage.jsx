import React from "react";
import Home from "../components/content/Home";
import Keyboard from "../components/keyboard/Keyboard";
import Header from "../components/header/Header";
import Footer from "../components/footer/Footer";


const Homepage = ({productItems,cartItems,addToCart,productKCItems}) => {
    return (
        <div>
            {/* Xóa cartItems={cartItems} của Header và Home */}
            <Header cartItems={cartItems}/>
            <Home/>
            <Keyboard productItems = {productItems} productKCItems={productKCItems} addToCart = {addToCart}/>
            <Footer/>
        </div>
    );
};

export default Homepage;