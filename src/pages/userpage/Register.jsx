import React, { useRef, useState, useEffect } from 'react';
import './Userpage.css';
import { faCheck, faInfoCircle, faXmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Swal from 'sweetalert2';
import axios from '../../api/axios';
import { Link,useNavigate } from 'react-router-dom';


const USER_NAME_REGEX = /^[a-zA-Z][a-zA-Z0-9-_]{5,}$/;
const PASSWORD_REGEX = /^(?=.*[A-Z])(?=.*\d)(?=.*\W).{6,}$/;
const REGISTER_URL = "/register";

const Register = () => {
    const userRef = useRef();
    const errRef = useRef();
    const navigate = useNavigate();

    // LOGIN NAME
    const [user, setUser] = useState('');
    const [validName, setValidName] = useState(false);
    const [userFocus, setUserFocus] = useState(false);

    //USER NAME
    const [userName ,setUserName] = useState('');

    // PASSWORD
    const [password, setPassword] = useState('');
    const [validPassword, setValidPassword] = useState(false);
    const [passwordFocus, setPasswordFocus] = useState(false);

    // CONFIRM PASSWORD
    const [confirmPassword, setConfirmPassword] = useState('');
    const [validConfirmPassword, setValidConfirmPassword] = useState(false);
    const [confirmPasswordFocus, setConfirmPasswordFocus] = useState(false);

    // ERROR MESSAGE
    const [errMessage, setErrorMessage] = useState('');

    useEffect(() => {
        userRef.current.focus();
    }, []);

    useEffect(() => {
        const result = USER_NAME_REGEX.test(user);
        console.log(result);
        console.log(user);
        setValidName(result);
    }, [user]);

    useEffect(() => {
        const result = PASSWORD_REGEX.test(password);
        console.log(result);
        console.log(password);
        setValidPassword(result);
        const checkMatch = password === confirmPassword;
        setValidConfirmPassword(checkMatch);
    }, [password, confirmPassword]);

    useEffect(() => {
        setErrorMessage('');
    }, [user, password, confirmPassword]);

    // XỬ LÝ ĐĂNG KÝ
    const handleSubmitRegis = async (e) => {
        e.preventDefault();

        const data = {
            nameUser: userName,
            loginUser: user,
            passwordUser: password
        };

        try{
            const response = await axios.post(REGISTER_URL,data);
            console.log(response.data);
            Swal.fire(
                'Good job!',
                'Đăng kí thành công!',
                'success'
              )
            navigate('/login');
        }catch(err){
            setErrorMessage("Đăng kí thất bại");
        }
    };

    return (
        <>
            <div className="form_body">
                <div className="handle_user">
                    <section className='register user_form'>
                    <p ref={errRef} className={errMessage ? "errMessage" : "d-none"} aria-live="assertive">{errMessage}</p>
                        <h2>Signup</h2>
                        <form onSubmit={handleSubmitRegis}>


                            <label htmlFor="loginuser">
                                Tên đăng nhập
                                <span className={validName ? "valid ml-2" : "d-none"}>
                                    <FontAwesomeIcon icon={faCheck} style={{ color: "#00ff40", }} />
                                </span>
                                <span className={validName || !user ? "d-none" : "invalid ml-2"}>
                                    <FontAwesomeIcon icon={faXmark} style={{ color: "#ff0000", }} />
                                </span>
                            </label>
                            <input type="text"
                                id='loginuser'
                                ref={userRef}
                                autoComplete='off'
                                onChange={(e) => setUser(e.target.value)} required
                                aria-invalid={validName ? "false" : "true"}
                                aria-describedby='usernote'
                                onFocus={() => setUserFocus(true)}
                                onBlur={() => setUserFocus(false)}
                                placeholder='Enter Username' name='loginUser' />
                            <p id='usernote' className={userFocus && user && !validName ? "instructions" : "d-none"}>
                                <FontAwesomeIcon icon={faInfoCircle} />
                                Tối thiểu 6 ký tự.<br />
                                Bắt đầu bằng một chữ cái.<br />
                                Chữ thường,hoa,số,-,_ đều được phép.
                            </p>


                            <label htmlFor="username">Tên tài khoản</label>
                            <input type="text"
                                id='username'
                                ref={userRef}
                                autoComplete='off'
                                onChange={(e) => setUserName(e.target.value)} required
                                placeholder='Enter Your Name' name='nameUser' />


                            <label htmlFor="userpassword">
                                Mật khẩu
                                <span className={validPassword ? "valid ml-2" : "d-none"}>
                                    <FontAwesomeIcon icon={faCheck} style={{ color: "#00ff40", }} />
                                </span>
                                <span className={validPassword || !password ? "d-none" : "invalid ml-2"}>
                                    <FontAwesomeIcon icon={faXmark} style={{ color: "#ff0000", }} />
                                </span>
                            </label>
                            <input type="password"
                                id='userpassword'
                                onChange={(e) => setPassword(e.target.value)} required
                                aria-invalid={validPassword ? "false" : "true"}
                                aria-describedby='passnote'
                                onFocus={() => setPasswordFocus(true)}
                                onBlur={() => setPasswordFocus(false)}
                                placeholder='Create Password' name='passwordUser' />
                            <p id='passnote' className={passwordFocus && password && !validPassword ? "instructions" : "d-none"}>
                                <FontAwesomeIcon icon={faInfoCircle} />
                                Tối thiểu 6 ký tự.<br />
                                Bắt đầu bằng chữ hoa.<br />
                                Mật khẩu nên chứa chữ thường và hoa.<br />
                                Ký tự đặc biệt được phép.
                            </p>


                            <label htmlFor="confirmpass">
                                Nhập lại mật khẩu
                                <span className={validConfirmPassword && confirmPassword ? "valid ml-2" : "d-none"}>
                                    <FontAwesomeIcon icon={faCheck} style={{ color: "#00ff40", }} />
                                </span>
                                <span className={validConfirmPassword || !confirmPassword ? "d-none" : "invalid ml-2"}>
                                    <FontAwesomeIcon icon={faXmark} style={{ color: "#ff0000", }} />
                                </span>
                            </label>
                            <input type="password"
                                id='confirmpass'
                                onChange={(e) => setConfirmPassword(e.target.value)} required
                                aria-invalid={validConfirmPassword ? "false" : "true"}
                                aria-describedby='confirmnote'
                                onFocus={() => setConfirmPasswordFocus(true)}
                                onBlur={() => setConfirmPasswordFocus(false)}
                                placeholder='Confirm Password' />
                            <p id='confirmnote' className={confirmPasswordFocus && !validConfirmPassword ? "instructions" : "d-none"}>
                                <FontAwesomeIcon icon={faInfoCircle} />
                                Mật khẩu không khớp.
                            </p>
                            <button disabled={!validName || !validPassword || !validConfirmPassword ? true : false}>Signup</button>
                        </form>
                        <div className="signup">
                            <span className="signup">Đã có tài khoản!

                                <Link to="/login">
                                    <label htmlFor="check">Login</label>
                                </Link>
                            </span>
                        </div>
                    </section>
                </div>
            </div>
        </>
    )
}

export default Register