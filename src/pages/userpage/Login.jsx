import React, { useEffect, useRef, useState } from 'react'
import './Userpage.css';
import { Link, useNavigate } from 'react-router-dom';
import axios from '../../api/axios';
import Swal from 'sweetalert2';

const LOGIN_URL = "/login";

const Login = () => {
    const loginRef = useRef();
    const navigate = useNavigate();

    const [login, setLogin] = useState('');
    const [loginpass, setLoginPass] = useState('');
    const [errMessage, setErrorMessage] = useState('');

    useEffect(() => {
        loginRef.current.focus();
    }, []);

    useEffect(() => {
        setErrorMessage('');
    }, [login, loginpass])

    //XỬ LÝ ĐĂNG NHẬP
    const handleSubmitLogin = async (e) => {
        e.preventDefault();

        const data = {
            loginUser: login,
            passwordUser: loginpass
        };

        try {
            const response = await axios.post(LOGIN_URL, data);

            if (response.status === 200) {
                navigate('/');
                Swal.fire(
                    'Good job!',
                    'Đăng nhập thành công!',
                    'success'
                )
                console.log(response.data);
                localStorage.setItem('isLoggedIn', true);
                localStorage.setItem('login', login);
                localStorage.setItem('loginpass', loginpass);

            } else {
                setErrorMessage(response.data);
            }
        } catch (err) {
            setErrorMessage("Đăng nhập thất bại")
        }
    }

    return (
        <>
            <div className="form_body">
                <div className="handle_user">
                    <section className='login user_form'>
                        <p>{errMessage}</p>
                        <h2>Login</h2>
                        <form onSubmit={handleSubmitLogin}>


                            <label htmlFor="loginname">Tên đăng nhập</label>
                            <input type="text"
                                id='loginname'
                                ref={loginRef}
                                autoComplete='off'
                                onChange={(e) => setLogin(e.target.value)}
                                value={login} required
                                placeholder='Enter Username' />

                            <label htmlFor="loginpass">Mật khẩu</label>
                            <input type="password"
                                id='loginpass'
                                onChange={(e) => setLoginPass(e.target.value)}
                                value={loginpass} required
                                placeholder='Enter Password' />

                            <p>Quên mật khẩu</p>
                            <button>Login</button>
                        </form>
                        <div className="signup">
                            <span className="signup">Không có tài khoản?
                                <Link to="/register">
                                    <label htmlFor="check">Signup</label>
                                </Link>
                            </span>
                        </div>

                    </section>
                </div>
            </div>
        </>
    )
}

export default Login