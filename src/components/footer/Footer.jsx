import React from 'react'
import "./Footer.css"
const Footer = () => {
    return (
        <div>
            <footer className='mt-5'>
                <div className='container grid2'>
                    <div className='box'>
                        <h1>Sói Gear</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor libero id et, in gravida. Sit diam duis mauris nulla cursus. Erat et lectus vel ut sollicitudin elit at amet.</p>
                        <div className='icon d_flex'>
                            <div className='img d_flex'>
                                <i class='fa-brands fa-google-play'></i>
                                <span>Google Play</span>
                            </div>
                            <div className='img d_flex'>
                                <i class='fa-brands fa-app-store-ios'></i>
                                <span>App Store</span>
                            </div>
                        </div>
                    </div>

                    <div className='box'>
                        <h2>Theo dõi chúng tôi</h2>
                        <ul>
                            <li>Trang chủ</li>
                            <li>Group Buy</li>
                            <li>Kiến thức</li>
                            <li>Chính sách bảo hành</li>
                            <li>Tuyển dụng</li>
                        </ul>
                    </div>
                    <div className='box'>
                        <h2>Hỗ trợ khách hàng</h2>
                        <ul>
                            <li>Hỗ trợ</li>
                            <li>Bảo hành</li>
                            <li>Hoàn trả & Hoàn tiền</li>
                        </ul>
                    </div>
                    <div className='box'>
                        <h2>Liên hệ</h2>
                        <ul>
                            <li>Trần Cao Vân - Thanh Khê - Đà Nẵng - Việt Nam </li>
                            <li>Email: longttham20@gmail.com</li>
                            <li>Phone: 0987654321</li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    )
}

export default Footer