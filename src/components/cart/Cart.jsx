import React, { useState } from "react";
import "./Cart.css"
import Header from "../header/Header";
import Footer from "../footer/Footer";
import { Link } from "react-router-dom";



const Cart = ({ cartItems, increaseQty, decreaseQty, removeCartItem }) => {
    const [itemQtys, setItemQtys] = useState(cartItems.map(item => item.qty));

    const totalPrice = cartItems.reduce((priceProduct, item, index) => priceProduct + itemQtys[index] * item.priceProduct, 0);

    // Xử lý tăng giảm
    const handleIncreaseQty = (item, index) => {
        const newQtys = [...itemQtys];
        newQtys[index] = itemQtys[index] + 1;
        setItemQtys(newQtys);
        increaseQty(item, newQtys[index]);
    };

    const handleDecreaseQty = (item, index) => {
        if (itemQtys[index] === 1) {
            removeCartItem(item);
        } else {
            const newQtys = [...itemQtys];
            newQtys[index] = itemQtys[index] - 1;
            setItemQtys(newQtys);
            decreaseQty(item, newQtys[index]);
        }
    };


    const handleTakeData = () => {
        const getItemsInCart = cartItems.map((item,index) =>({
            idProduct : item.idProduct,
            nameProduct : item.nameProduct,
            priceProduct : item.priceProduct,
            qty : itemQtys[index],
            src : item.src,
            sale : item.sale
        }))
        localStorage.setItem('totalPrice',totalPrice);
        localStorage.setItem('getItemsInCart', JSON.stringify(getItemsInCart));
    }
   
    console.log(cartItems)
    return (
        <div>
            <Header cartItems={cartItems} />
            <section className="cart-items">
                <div className="container d-flex">
                    <div className="cart-details">
                        {cartItems.length === 0 && <h1 className="no-item product">Khong co san pham</h1>}

                        {cartItems.map((item, index) => {
                            const productQty = item.priceProduct * itemQtys[index];
                            return (
                                <div className="cart-list product d-flex" key={item.idProduct}>
                                    <div className="img">
                                        <img src={item.src} alt="" />
                                    </div>
                                    <div className="cart-details ml-3">
                                        <h3>{item.nameProduct}</h3>
                                        <h4>
                                            Đơn giá : {item.priceProduct} <br />
                                            Số lượng : {itemQtys[index]} <br />
                                            Thành tiền : <span>{productQty}</span>
                                        </h4>
                                    </div>
                                    <div className="cart-items-function">
                                        <div className="removeCart">
                                            <button className="removeCart" onClick={() => removeCartItem(item)}>
                                                <i class="fa-regular fa-square-xmark"></i>
                                            </button>
                                        </div>
                                        <div className="cartControl d-flex">
                                            <button className="desCart" onClick={() => handleDecreaseQty(item, index)}>
                                                <i className="fa fa-minus"></i>
                                            </button>
                                            <input type="number"
                                                value={itemQtys[index]}
                                                min={1}
                                                onChange={(e) => {
                                                    const newQty = parseInt(e.target.value, 10);
                                                    if (!isNaN(newQty)) {
                                                        const newQtys = [...itemQtys];
                                                        newQtys[index] = newQty;
                                                        setItemQtys(newQtys);
                                                    }
                                                }}
                                            />
                                            <button className="incCart" onClick={() => handleIncreaseQty(item, index)}>
                                                <i className="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div className="cart-item-price"></div>
                                </div>

                            )
                        })}
                    </div>
                    <div className="cart-total product">
                        <h2>Tổng giá trị đơn hàng</h2>
                        <div className="d-flex">
                            <h4>Thành tiền : </h4>
                            <h3>
                                {totalPrice}
                            </h3>
                        </div>
                        <div className="paycheck mt-5">
                            <Link to={"/paycheck"}>
                                <button className="btn btn-warning pay-button" onClick={handleTakeData}>
                                    Thanh toán
                                </button>
                            </Link>
                        </div>
                    </div>

                </div>
            </section>
            <Footer />
        </div>
    );
}

export default Cart;