const Sliderdata = [
    {
        id : 1,
        title : "Coming soon",
        desc : "Lorem ipsum dolor sit amet, consectetur adip",
        src : "/images/slider/slider_01.png"
    },
    {
        id : 2,
        title : "Hot Sales",
        desc : "Lorem ipsum dolor sit amet, consectetur adip",
        src : "/images/slider/slider_02.png"
    },
    {
        id : 3,
        title : "Newest",
        desc : "Lorem ipsum dolor sit amet, consectetur adip",
        src : "/images/slider/slider_03.png"
    }
]

export default Sliderdata;