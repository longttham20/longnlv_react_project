import React from 'react';
import Slider from 'react-slick';
import Sliderdata from '../content/Sliderdata.js';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const SlideCard = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    appendDots: (dots) => {
      return <ul style={{ margin: "0px" }}>{dots}</ul>
    }
  }
  return (
    <div>
      <Slider {...settings}>
        {Sliderdata.map((value, index) => {
          return (
            <div className="box d-flex top" key={index}>
              <div className="left">
                <h1>{value.title}</h1>
                <p>{value.desc}</p>
                <button className='btn-primary'>Join Now</button>
              </div>
              <div className="right">
                <img src={value.src} alt="" />
              </div>
            </div>
          )
        })}
      </Slider>
    </div>
  );

};

export default SlideCard;