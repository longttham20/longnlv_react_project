import React from "react"

const Categories = () => {
  const data = [
    {
      cateImg: "/images/categories/zoom65v2-exploded2-vuong.png",
      cateName: "Bàn phím cơ",
    },
    {
      cateImg: "/images/categories/15307-cherry-mx-keycap-r2-translucent-black-01.png",
      cateName: "Keycap",
    },
    {
      cateImg: "/images/categories/pk.png",
      cateName: "Phụ kiện",
    }
  ]

  return (
    <>
      <div className='category'>
        {data.map((value, index) => {
          return (
            <div className='box f_flex' key={index}>
              <img src={value.cateImg} alt='' />
              <span>{value.cateName}</span>
            </div>
          )
        })}
      </div>
    </>
  )
}

export default Categories