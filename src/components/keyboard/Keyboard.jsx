import React from "react";
import KeyboardCard from "./KeyboardCard";
import KeycapCard from "./KeycapCard";

const Keyboard = ({ addToCart }) => {
    return (
        <div>
            <section className="keyboard background">
                <div className="container">
                    <div className="heading f-flex justify-content-center mt-4">
                        <i class="fa-regular fa-keyboard" style={{ fontSize: '30px' }}></i>
                        <h2>Bàn Phím Cơ</h2>
                    </div>
                    <div>
                        <KeyboardCard addToCart={addToCart} />
                    </div>
                    <div className="heading f-flex justify-content-center mt-4">
                        <i class="fa-regular fa-keyboard" style={{ fontSize: '30px' }}></i>
                        <h2>Keycap</h2>
                    </div>
                    <div>
                        <KeycapCard addToCart={addToCart}/>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default Keyboard;