import React, { useEffect, useState } from "react";
import Slider from "react-slick"
import { Link } from "react-router-dom";
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import axios from "../../api/axios";

const KeyboardCard = ({ addToCart }) => {
    const [keyboardItems, setKeyboardItems] = useState([]);
    const KEYBOARD_DATA_URL = "http://localhost:8080/keyboard/";
    useEffect(() => {
        fetchData();
    },[]);

    const fetchData = async () => {
        try {
            const response = await axios.get(KEYBOARD_DATA_URL);
            setKeyboardItems(response.data);
        }catch(err){
            console.error("Error fetching",err);
        }
    };

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slideToScroll: 1,
    }
    return (
        <div>
            <Slider {...settings}>
                {keyboardItems.map((keyboardItem) => {
                    return (
                        <div className="box" key={keyboardItem.idProduct}>
                            <div className="product mtop">
                            <Link to={`/product/${keyboardItem.idProduct}`} className="text-decoration-none">
                                <div className="img" key={keyboardItem.idProduct}>
                                    <span className="discount">{keyboardItem.sale}% Off </span>
                                    <img src={keyboardItem.src} alt="" />
                                    <div className="product-like">
                                        <label>0</label> <br />
                                        <i className="fa-regular fa-heart"></i>
                                    </div>
                                </div>
                                </Link>
                                <div className="product-details">
                                    <h3>{keyboardItem.nameProduct}</h3>
                                    <div className="rate">
                                        <i className='fa fa-star'></i>
                                        <i className='fa fa-star'></i>
                                        <i className='fa fa-star'></i>
                                        <i className='fa fa-star'></i>
                                        <i className='fa fa-star'></i>
                                    </div>
                                    <div className="price">
                                        <h4>{keyboardItem.priceProduct}</h4>
                                        <button onClick={() => addToCart(keyboardItem)}>
                                            <i className="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </Slider>
        </div>
    );
};

export default KeyboardCard;
