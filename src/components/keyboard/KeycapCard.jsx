import React, { useEffect, useState } from "react";
import Slider from "react-slick"
import { Link } from "react-router-dom";
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import axios from "../../api/axios";

const KeycapCard = ({ addToCart }) => {
    const [keycapItems, setKeycapItems] = useState([]);
    const KEYCAP_DATA_URL = "http://localhost:8080/keycap/";
    useEffect(() => {
        fetchData();
    },[]);

    const fetchData = async () => {
        try {
            const response = await axios.get(KEYCAP_DATA_URL);
            setKeycapItems(response.data);
        }catch(err){
            console.error("Error fetching",err);
        }
    };
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slideToScroll: 1,
    }
    return (
        <div>
            <Slider {...settings}>
                {keycapItems.map((keycapItem) => {
                    return (
                        <div className="box">
                            <div className="product mtop">
                            <Link to={`/product/${keycapItem.idProduct}`} className="text-decoration-none">
                                <div className="img" key={keycapItem.idProduct}>
                                    <span className="discount">{keycapItem.sale}% Off</span>
                                    <img src={keycapItem.src} alt="" />
                                    <div className="product-like">
                                        <label>0</label> <br />
                                        <i className="fa-regular fa-heart"></i>
                                    </div>
                                </div>
                            </Link> 
                                <div className="product-details">
                                    <h3>{keycapItem.nameProduct}</h3>
                                    <div className="rate">
                                        <i className='fa fa-star'></i>
                                        <i className='fa fa-star'></i>
                                        <i className='fa fa-star'></i>
                                        <i className='fa fa-star'></i>
                                        <i className='fa fa-star'></i>
                                    </div>
                                    <div className="price">
                                        <h4>{keycapItem.priceProduct}</h4>
                                        <button onClick={() => addToCart(keycapItem)}>
                                            <i className="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </Slider>
        </div>
    );
};

export default KeycapCard;
