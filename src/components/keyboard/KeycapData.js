const KeycapData = {
    productKCItems: [
      {
        id: 11,
        discount: 10,
        src: "/images/keycap/keycap1.png",
        name: "Keycap1",
        price: 100,
      },
      {
        id: 12,
        discount: 20,
        src: "/images/keycap/keycap2.png",
        name: "Keycap2",
        price: 20,
      },
      {
        id: 13,
        discount: 30,
        src: "/images/keycap/keycap3.png",
        name: "Keycap3",
        price: 200,
      },
      {
        id: 14,
        discount: 40,
        src: "/images/keycap/keycap4.png",
        name: "Keycap4",
        price: 50,
      },
      {
        id: 15,
        discount: 50,
        src: "/images/keycap/keycap5.png",
        name: "Keycap5",
        price: 100,
      },
    ],
  }
  export default KeycapData;