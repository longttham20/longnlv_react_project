const KeyboardData = {
    productItems: [
      {
        id: 1,
        discount: 50,
        src: "/images/keyboard/keyboard1.png",
        name: "Keyboard1",
        price: 100,
      },
      {
        id: 2,
        discount: 40,
        src: "/images/keyboard/keyboard2.png",
        name: "Keyboard2",
        price: 20,
      },
      {
        id: 3,
        discount: 40,
        src: "/images/keyboard/keyboard3.png",
        name: "Keyboard3",
        price: 200,
      },
      {
        id: 4,
        discount: 40,
        src: "/images/keyboard/keyboard4.png",
        name: "Keyboard4",
        price: 50,
      },
      {
        id: 5,
        discount: 50,
        src: "/images/keyboard/keyboard5.png",
        name: "Keyboard5",
        price: 100,
      },
      {
        id: 6,
        discount: 50,
        src: "/images/keyboard/keyboard6.png",
        name: "Keyboard6",
        price: 100,
      },
    ],
  }
  export default KeyboardData;