import React, { useState } from "react";
import logo from "../../assets/logo.png"
import { Link,useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

const Search = ({ cartItems }) => {
    const itemQty = cartItems.reduce((number, item) => number + item.qty, 0);
    const navigate = useNavigate();

    const [showDropdown, setShowDropdown] = useState(false);
    
    const loginLocal = localStorage.getItem('login');
    const isLoggedIn = localStorage.getItem('isLoggedIn');
    
    const handleLogout = () => {
        localStorage.removeItem('isLoggedIn');
        localStorage.removeItem('login');
        navigate('/');
        Swal.fire(
            'Good job!',
            'Đăng xuất thành công!',
            'success'
        )

    }

    const handleNavigetAccount = () => {
        navigate(`/account/${loginLocal}`)
    }

    return (
        <section className="search mt-2 d-flex">
            <div className="container c-flex">
                <div className="logo width">
                    <img src={logo} alt="pagelogo" />
                </div>
            </div>

            <div className="search-box f-flex">
                <i className="fa fa-search"></i>
                <input type="text" placeholder="Nhap ten san pham" />
            </div>

            <div className="icon f-flex width">
                <div className="user">
                    {!isLoggedIn ? (
                        <Link to="/login">
                            <i className="fa fa-user icon-circle"></i>
                        </Link>
                    ) : (
                        <div className="isLoggin" onClick={() => 
                        setShowDropdown((prev) => !prev)} onBlur={() => setShowDropdown(false)}
                        tabIndex={0}>
                            <i className="fa fa-user icon-circle"></i>
                            {showDropdown && (
                                <div className="dropDown bg-secondary position-absolute p-2">
                                    <div className="btn btn-info mr-2" onClick={handleNavigetAccount}>{loginLocal}</div>
                                    <div className="btn btn-danger" onClick={handleLogout}>Logout</div>
                                </div>
                            )}
                        </div>
                    )}
                </div>
                <div className="cart">
                    <Link to="/cart">
                        <i className="fa fa-shopping-bag icon-circle"></i>
                        <span>{itemQty}</span>
                    </Link>
                </div>
            </div>
        </section>
    );
};

export default Search;