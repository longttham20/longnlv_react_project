import React, { useState } from "react";
import { Link } from "react-router-dom";

const Navbar = () => {

    const [MobileMenu, setMobileMenu] = useState(false)

    return (
        <header className="header">
            <div className="container d-flex">
                <div className="categories d-flex">
                    <span className="fa-solid fa-border-all"></span>
                    <h4>
                        Danh Mục
                    </h4>
                </div>

                <div className="navlink">
                    <ul className={MobileMenu ? "nav-links-MobileMenu" : "link f-flex capitalize"}
                        onClick={() => setMobileMenu(false)}>
                        <li>
                            <Link to="/">Trang chủ</Link>
                        </li>
                        <li>
                            <span>Bảo Hành</span>
                        </li>
                        <li>
                            <span>Tuyển dụng</span>
                        </li>
                        <li>
                            <Link to="/contact">Liên hệ</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    );
};

export default Navbar;