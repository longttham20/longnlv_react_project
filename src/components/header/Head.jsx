import React from "react";

const Head = () => {
    return (
       <section className="head">
            <div className="container d-flex">
                <div className="row left">
                    <i className="fa fa-phone"></i>
                    <label>0987654321</label>
                    <i className="fa fa-envelope"></i>
                    <label>longttham20@gmail.com</label>
                </div>
                <div className="row right RText">
                    <label>LONGLV</label>
                    <label>FR_JAVA_2304</label>
                </div>
            </div>
       </section>
    );
};

export default Head;