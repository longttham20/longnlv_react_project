import React from "react";
import Head from "./Head";
import Search from "./Search";
import Navbar from "./Navbar";
import "./Header.css";

const Header = ({cartItems}) => {
    return (
        <>
            <Head/>
            <Search cartItems={cartItems}/>
            <Navbar/>
        </>
    );
};

export default Header;